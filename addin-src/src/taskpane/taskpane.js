/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

/* global document, Office, Word */
/* Library imports */
import { CosmosClient as cosmosclient } from "@azure/cosmos";
import config from "../config";

/* Get config values */
const endpoint = config.endpoint;
const key = config.masterkey;
const databaseId = config.databaseId;
const containerId = config.containerId;
const dbQuery = config.databaseQuery;
const targetDomain = config.targetDomain;

/* Get COSMOS client */
const client = new cosmosclient({
    endpoint,
    key,
    //connectionPolicy is required to disable CORS Policy
    connectionPolicy: {
        enableEndpointDiscovery: false
    }
});

/* Global Variable */
const varNotFount = "Link Skipped";
let arrHypLinks = [];
let tblSummery = document.getElementById("divTblSummery");

/* On ready of the Addin */
Office.onReady(info => {
    if (info.host === Office.HostType.Word) {
        document.getElementById("sideload-msg").style.display = "none";
        document.getElementById("app-body").style.display = "flex";
        document.getElementById("btn-slected-area").onclick = findFixOfSelectedArea;
        document.getElementById("btn-full-doc").onclick = findFixOfWholeDoc;
    }
});

/*
 * Async method - Get all the Hyperlinks of the entire document without manual selection
 * Query COSMOS DB - For each old link, get the new link from the COSMOS DB
 */
async function findFixOfWholeDoc() {
    arrHypLinks = [];
    tblSummery.innerHTML = "";
    //Disable the buttons if clicked for whole document
    document.getElementById("btn-slected-area").disabled = true;
    document.getElementById("btn-full-doc").disabled = true;

    await Word.run(function(context) {
        var fullDoc = context.document.body.getRange('Whole');
        context.load(fullDoc, "hyperlink");
        return context.sync().then(function() {
            let hyperlinks = fullDoc.getHyperlinkRanges();
            hyperlinks.load("hyperlink");
            return context.sync().then(function() {
                queryCosmosDB(hyperlinks.items).then(function() {
                    replaceBrokenLinks(1);
                });
            });
        });
    }).catch(function(error) {
        console.log("Error in findFixOfWholeDoc: " + error);
        if (error instanceof OfficeExtension.Error) {
            console.log("Debug info: " + JSON.stringify(error.debugInfo));
        }
    });
}

/*
 * Async method - Get all the Hyperlinks within the selected user selected area of the document
 * Query COSMOS DB - For each old link, get the new link from the COSMOS DB
 */
async function findFixOfSelectedArea() {
    arrHypLinks = [];
    tblSummery.innerHTML = "";

    await Word.run(function(context) {
        //var searchResults = context.document.body.search("https", { matchSuffix: true });    
        let selectedDoc = context.document.getSelection();
        context.load(selectedDoc, "hyperlink");
        return context.sync().then(function() {
            let hyperlinks = selectedDoc.getHyperlinkRanges();
            hyperlinks.load("hyperlink");
            return context.sync().then(function() {
                queryCosmosDB(hyperlinks.items).then(function() {
                    replaceBrokenLinks(0);
                });
            });
        });
    }).catch(function(error) {
        console.log("Error in findFixOfSelectedArea: " + error);
        if (error instanceof OfficeExtension.Error) {
            console.log("Debug info: " + JSON.stringify(error.debugInfo));
        }
    });
}

/*
 * Async method - For each hyperlinks, query COSMOS DB
 * Get the new link & append the array
 */
async function queryCosmosDB(array) {
    for (const item of array) {
        const querySpec = {
            query: dbQuery,
            parameters: [{
                name: "@sourceurl",
                value: item.hyperlink
            }]
        };

        //Query to COSMOS
        const { resources } = await client.database(databaseId).container(containerId).items.query(querySpec).fetchAll();
        //const { resources } = await storageClient.database(databaseId).container(containerId).items.query(querySpec).fetchAll();
        //Set the Global variable -- if no matching link, then set NA
        let strTarget = varNotFount;
        if (resources.length > 0)
            strTarget = resources[0].targeturl;

        arrHypLinks.push({ key: item.hyperlink, value: strTarget });
    }
}

/*
 * Async method - Get all the Hyperlinks within the selected part of the document once again
 * Replace the links with the respective values returned from the COSMOS DB
 * As the context of the previous same operation was lost due to the Async call to COSMOS DB, 
 * so again need to buld the context for update
 */
async function replaceBrokenLinks(isFull) {
    await Word.run(function(context) {
        let selectedDoc;
        if (isFull > 0)
            selectedDoc = context.document.body.getRange('Whole');
        else
            selectedDoc = context.document.getSelection();
        context.load(selectedDoc, "hyperlink, text");
        return context.sync().then(function() {
            let hyperlinks = selectedDoc.getHyperlinkRanges();
            hyperlinks.load("hyperlink, text");
            return context.sync().then(function() {

                //Start building the summery
                var paraSummery = document.createElement("p");
                var pNode = document.createTextNode("Summery:");
                paraSummery.appendChild(pNode);
                tblSummery.appendChild(paraSummery);

                if (arrHypLinks.length > 0) {
                    //Build Summery Table Structure                                   
                    let table = document.createElement('TABLE');
                    let tableBody = document.createElement('TBODY');
                    table.appendChild(tableBody);

                    let th = document.createElement('TR');
                    tableBody.appendChild(th);

                    let thtd1 = document.createElement('TD');
                    thtd1.colSpan = 1;
                    thtd1.style.textAlign = "center";
                    thtd1.style.fontWeight = "1000";
                    thtd1.appendChild(document.createTextNode("Link Text"));
                    th.appendChild(thtd1);

                    let thtd2 = document.createElement('TD');
                    thtd2.colSpan = 1;
                    thtd2.style.textAlign = "center";
                    thtd2.style.fontWeight = "1000";
                    thtd2.appendChild(document.createTextNode("Replaced Link"));
                    th.appendChild(thtd2);
                    //End

                    hyperlinks.items.forEach(function(item) {
                        arrHypLinks.forEach(function(i) {
                            if (i.key === item.hyperlink) {
                                if (i.value !== varNotFount) {
                                    item.hyperlink = i.value;
                                }

                                //Collect summery
                                let tr = document.createElement('TR');
                                tableBody.appendChild(tr);

                                let td1 = document.createElement('TD');
                                td1.colSpan = 1;
                                td1.appendChild(document.createTextNode(item.text));
                                tr.appendChild(td1);

                                let td2 = document.createElement('TD');
                                td2.colSpan = 1;
                                td2.appendChild(document.createTextNode(i.value));
                                tr.appendChild(td2);
                            }
                        });
                    });

                    //Append body to the div table
                    tblSummery.appendChild(table);
                } else {
                    var label = document.createElement("LABEL");
                    var node = document.createTextNode("No effect has taken place !");
                    label.appendChild(node);
                    tblSummery.appendChild(label);
                }

                return context.sync();
            });
        });
    }).catch(function(error) {
        console.log("Error replaceBrokenLinks: " + error);
        if (error instanceof OfficeExtension.Error) {
            console.log("Debug info: " + JSON.stringify(error.debugInfo));
        }
    });
}