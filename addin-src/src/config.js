/*
 * Configuration file to store the Azure Cosmos DB metadata
 * See LICENSE in the project root for license information.
 */
var config = {
    endpoint: "https://XXXXXXXXX.documents.azure.com:443/",
    masterkey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    databaseId: "XXXXXXXXXX",
    containerId: "XXXXXXXXXXXXXXXXXXX",
    databaseQuery: "SELECT c.sourceurl, c.targeturl FROM c WHERE c.sourceurl = @sourceurl",
    targetDomain: ".sharepoint.com/"
};

export default config;